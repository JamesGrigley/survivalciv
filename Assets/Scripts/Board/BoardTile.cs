﻿using UnityEngine;
using System.Collections;

public class BoardTile : MonoBehaviour {

    Renderer renderer;
    Material selectedMaterial;
    public int positionX;
    public int positionY;

    public enum tileTypeEnum : int {Land, Water}

    public tileTypeEnum tileType;

    bool hasFog = true;

    [System.NonSerialized]
    public bool hasFire = false;

    public BaseUnit unit
    {
        get
        {
            return transform.GetComponentInChildren<BaseUnit>(true);
        }
    }

    public BaseBuilding building
    {
        get
        {
            return transform.GetComponentInChildren<BaseBuilding>(true);
        }
    }

    bool canMove = false;
    bool canAttack = false;

    void Awake()
    {
        renderer = GetComponent<Renderer>();

    }

    void Start() {
        setMaterial();

    }

    void OnMouseDown()
    {
        //Debug.Log("Tile was clicked");
        click();
    }

    public void click()
    {
        if (canMove)
        {
            moveUnit();
        }
        else if (canAttack)
        {
            UnitManager.unitManager.attackTile(this);
            if(!unit)
                moveUnit();
        }

    }

    void setMaterial()
    {
        if (canMove)
            setMaterial(materialEnum.Green, true);
        else if (canAttack)
            setMaterial(materialEnum.Yellow, true);
        else if (hasFog)
        {
            setMaterial(materialEnum.Black);
        }
        else if (tileType == tileTypeEnum.Land)
        {
            setMaterial(materialEnum.White);
        }
        else if (tileType == tileTypeEnum.Water)
        {
            setMaterial(materialEnum.Blue);
        }
    }


    public void setMaterial(materialEnum material, bool isTempColor = false)
    {
        if (!isTempColor)
            selectedMaterial = Helper.helper.material(material);
        renderer.sharedMaterial = Helper.helper.material(material);

        if(material == materialEnum.Black && unit)
        {
            unit.visible = false;
        }
        else if(material == materialEnum.White && unit)
        {
            unit.visible = true;
        }

    }

    public void toggleMoveHighlight(bool toggle)
    {
        canMove = toggle && unit == null && tileType != tileTypeEnum.Water;

        setMaterial();
    }

    public void toggleAttackHighlight(bool toggle)
    {
        canAttack = toggle && unit && unit.PlayerID != 1;

        setMaterial();
    }


    public void clearFog()
    {
        hasFog = false;
        setMaterial();
    }

    void moveUnit()
    {
        UnitManager.unitManager.moveUnit(this);
    }
}
