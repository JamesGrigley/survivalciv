﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour
{

    public static Board board;

    BoardTile[,] boardTiles;
    int tilesX = 20;
    int tilesY = 20;
    float tileSize = 1;

    Transform boardTilePrefab;

    void Awake()
    {
        board = this;

        boardTilePrefab = Resources.Load<Transform>("Prefabs/BoardTile");
    }

    public void instantiateBoard()
    {
        //Debug.Log("Board Instansiated");
        boardTiles = new BoardTile[tilesX, tilesY];

        for (int x = 0; x < tilesX; x++)
        {
            for (int y = 0; y < tilesY; y++)
            {
                Transform tile = (Transform)Instantiate(boardTilePrefab, new Vector3(x, 1, y), Quaternion.identity);
                tile.parent = transform;
                boardTiles[x, y] = tile.GetComponent<BoardTile>();
                boardTiles[x, y].positionX = x;
                boardTiles[x, y].positionY = y;

            }
        }

        boardTiles[6, 6].tileType = BoardTile.tileTypeEnum.Water;

        Spawner.spawner.spawnBuilding(6, 5);

    }

    public void placeUnit(BaseUnit unit, int x, int y)
    {
       // Debug.Log("Place Unit");

        unit.transform.parent = boardTiles[x, y].transform;
        unit.transform.localPosition = Vector3.zero + Vector3.up * 2;
        clearFog(unit);
    }

    public void placeBuilding(BaseBuilding building, int x, int y)
    {
        // Debug.Log("Place Unit");

        building.transform.parent = boardTiles[x, y].transform;
        building.transform.localPosition = Vector3.zero;
    }

    public void moveUnit(BaseUnit unit, BoardTile tile)
    {
        unit.transform.parent = tile.transform;
        unit.move();
        clearFog(unit);
    }

    public void test(int id, int id2) { }

    public void toggleOptions(int toggleMode)
    {
        toggleOptions(false, 1);
        toggleOptions(true, toggleMode);
    }

    public void toggleOptions(bool toggle, int toggleMode = 1) // 1=all/2=moveonly/3=attackonly
    {
        BaseUnit unit = UnitManager.unitManager.selectedUnit;

        if (toggleMode == 1 || toggleMode == 2)
            toggleMoveOptions(unit, toggle);

        if (toggleMode == 1 || toggleMode == 3)
            toggleAttackOptions(unit, toggle);

    }

    void toggleMoveOptions(BaseUnit unit, bool toggle)
    {
        //Debug.Log("showMoveOptions");
        if (unit.PlayerID != 1) return;
        int x = unit.tile.positionX;
        int y = unit.tile.positionY;
        int r = unit.moveRange;

        for (int a = x-r;a <= x +r; a++)
        {
            for (int b = y - r; b <= y + r; b++)
            {
                if (a == x && b == y)
                {
                    // do nothing if is self
                }
                else if (a >= 0 && b >= 0 && a < tilesX && b < tilesY)
                    boardTiles[a, b].toggleMoveHighlight(toggle);
            }
        }
    }

    void toggleAttackOptions(BaseUnit unit, bool toggle)
    {
        //Debug.Log("showMoveOptions");
        if (unit.PlayerID != 1) return;
        int x = unit.tile.positionX;
        int y = unit.tile.positionY;
        int r = unit.attackRange;

        for (int a = x - r; a <= x + r; a++)
        {
            for (int b = y - r; b <= y + r; b++)
            {
                if (a == x && b == y)
                {
                    // do nothing if is self
                }
                else if (a >= 0 && b >= 0 && a < tilesX && b < tilesY)
                    boardTiles[a, b].toggleAttackHighlight(toggle);
            }
        }
    }

    void clearFog(BaseUnit unit)
    {
        //Debug.Log("clearFog");
        if (unit.PlayerID != 1) return;
        int x = unit.tile.positionX;
        int y = unit.tile.positionY;
        int r = unit.seeRange;

        for (int a = x - r; a <= x + r; a++)
        {
            for (int b = y - r; b <= y + r; b++)
            {
                if (a >= 0 && b >= 0 && a < tilesX && b < tilesY)
                    boardTiles[a, b].clearFog();
            }
        }
    }

    public bool isAdjacentToWater()
    {
        BaseUnit unit = UnitManager.unitManager.selectedUnit;
        int x = unit.tile.positionX;
        int y = unit.tile.positionY;
        int r = 1;

        for (int a = x - r; a <= x + r; a++)
        {
            for (int b = y - r; b <= y + r; b++)
            {
                if ((a >= 0 && b >= 0 && a < tilesX && b < tilesY) &&boardTiles[a, b].tileType == BoardTile.tileTypeEnum.Water)
                    return true;
            }
        }
        return false;
    }
}
