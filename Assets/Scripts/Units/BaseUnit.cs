﻿using UnityEngine;
using System.Collections;

public class BaseUnit : MonoBehaviour {
    Renderer renderer;

    private int playerID;
    public int ID;
    public string unitName;
    float speed = 20;

    public int health;
    public int maxHealth = 10;

    int attackDamage = 10;
    [System.NonSerialized]
    public int moveRange = 1;
    [System.NonSerialized]
    public int seeRange = 2;
    [System.NonSerialized]
    public int attackRange = 1;

     bool isMoving = false;

    public bool turnOver = false;

    public int woodCount = 0;
    public int dirtyWaterCount = 0;
    [System.NonSerialized]
    public int cleanWaterCount = 5;

    [System.NonSerialized]
    public int foodCount = 5;
    [System.NonSerialized]
    public int rawMeatCount = 0;

    public int turnOrder;

    public bool visible
    {
        get
        {
            return gameObject.activeSelf;
        }
        set
        {
            gameObject.SetActive(value);
        }
    }

    public int PlayerID
    {
        get
        {
            return playerID;
        }

        set
        {
            playerID = value;
            updateMaterial();
        }
    }

    public BoardTile tile
    {
        get
        {
            return transform.parent.GetComponent<BoardTile>();
        }
    }

    void Awake()
    {
        renderer = GetComponent<Renderer>();
        visible = false;
        health = maxHealth;

    }

    void Update()
    {
        if (!isMoving) return;

        float step = speed * Time.deltaTime;
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero + Vector3.up * 2, step);
        if (transform.localPosition == Vector3.zero)
        {
            isMoving = false;
        }

    }

    void OnMouseDown()
    {
        //Debug.Log(name + " was clicked");
        if (PlayerID == 1)
        {
            UnitManager.unitManager.selectUnit(this);
        }
        else
        {
            tile.click();
        }
    }


    void updateMaterial()
    {
        if (playerID == 1)
        {
            renderer.sharedMaterial = Helper.helper.material(materialEnum.Orange);
        }
        else if (playerID == 2)
        {
            renderer.sharedMaterial = Helper.helper.material(materialEnum.Red);
        }
    }


    public void move()
    {
        isMoving = true;
    }

    public void attack(BaseUnit unit)
    {
        unit.takeDamage(attackDamage);
        rawMeatCount += 5;
    }

    public void takeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            health = 0;
            die();
        }
    }

    void die()
    {
        UnitManager.unitManager.removeUnit(this);
        Destroy(this.gameObject);
    }

    public void select()
    {
        unitName = "Unit " + ID.ToString();
    }

}
