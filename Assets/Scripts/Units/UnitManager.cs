﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class UnitManager : MonoBehaviour {

    public static UnitManager unitManager;

    BaseUnit[] units;
    [System.NonSerialized]
    public BaseUnit selectedUnit;
    Transform hightlight;

    int turnOrderCount = 1;

    void Awake()
    {
        unitManager = this;

        units = new BaseUnit[20];

        hightlight = Instantiate<Transform>(Resources.Load<Transform>("Prefabs/Highlight"));
        hightlight.gameObject.SetActive(false);

    }

    public void loadStartUnits()
    {
        addUnit(2, 4, 4);
        addUnit(1, 1, 2);
        addUnit(1, 5, 5);
        //addUnit(1, 4, 2);


        selectUnit(getNextUnit(1));
    }

    void resortUnits()
    {
        turnOrderCount = 0;

        List<BaseUnit> unitList = new List<BaseUnit>();
        for(int x=0;x<units.Length;x++)
        {
            if (units[x] != null)
                unitList.Add(units[x]);
        }
            
        List<BaseUnit> sortedList = unitList.OrderBy(o => o.turnOrder).ToList();

        units = new BaseUnit[20];

        for (int x = 0; x < sortedList.Count; x++)
        {
            units[x] = sortedList[x];
        }
    }

    void addUnit(int playerID, int x, int y)
    {
        for (int i = 0; i < units.Length; i++)
        {
            if (units[i] == null)
            {
                units[i] = Spawner.spawner.spawnUnit(i + 1, playerID, x, y);
                break;
            }
        }
    }

    public void selectUnit(BaseUnit unit)
    {
        if (unit.turnOver) return;

        if(selectedUnit != null)
            Board.board.toggleOptions(false);

        unit.select();

        hightlight.gameObject.SetActive(true);
        hightlight.position = unit.transform.position + Vector3.up;

        selectedUnit = unit;
        CameraManager.cameraManager.focusOnObject(unit.transform);
        Board.board.toggleOptions(true);
        MainUI.mainUI.updateUnitUI();
    }

    public void moveUnit(BoardTile tile)
    {
        Board.board.toggleOptions(false);
        Board.board.moveUnit(selectedUnit, tile);
        hightlight.gameObject.SetActive(false);
        endUnitTurn();
        StartCoroutine(waitForMove());

    }

    public void attackTile(BoardTile tile)
    {
        Board.board.toggleOptions(false);
        selectedUnit.attack(tile.unit);
        Board.board.moveUnit(selectedUnit, tile);
        hightlight.gameObject.SetActive(false);
        endUnitTurn();
        MainUI.mainUI.updateUnitUI();
        StartCoroutine(waitForMove());
    }

    public void scavengeTile()
    {
        selectedUnit.woodCount++;
        Board.board.toggleOptions(false);
        hightlight.gameObject.SetActive(false);
        endUnitTurn();
        MainUI.mainUI.updateUnitUI();
        StartCoroutine(waitForMove(0.5f));

    }

    IEnumerator waitForMove(float delay = 1)
    {
        yield return new WaitForSeconds(delay);
        focusNextUnit();

    }

    public void removeUnit(BaseUnit unitToRemove)
    {
        for (int x = 0; x < units.Length; x++)
        {
            if (unitToRemove == units[x])
            {
                units[x] = null;
                break;
            }
        }
    }

    BaseUnit getNextUnit(int playerID)
    {
        //Debug.Log("getNextUnit");
        BaseUnit unit = null;
        for (int x = 0; x < units.Length; x++)
        {
            if (units[x] != null && units[x].PlayerID == playerID && units[x].turnOver == false)
            {
                unit = units[x];
                break;
            }
        }
        return unit;
    }

    public void newTurn()
    {
        resortUnits();

        for (int x = 0; x < units.Length; x++)
        {
            if (units[x] != null)
            {
                units[x].turnOver = false;
                units[x].cleanWaterCount--;
                units[x].foodCount--;

                if (units[x].cleanWaterCount < 0)
                {
                    units[x].cleanWaterCount = 0;
                    units[x].attack(units[x]);
                }
            }
        }
    }

    bool gotUnits = false;
    public void focusNextUnit()
    {
        BaseUnit unit = getNextUnit(1);
        if (unit != null)
        {
            selectUnit(unit);
            gotUnits = true;
        }
        else if(gotUnits)
        {
            gotUnits = false;
            GameplayController.gameplayController.resetTurn();
        }
        else
        {
            Debug.Log("GAME OVER");
        }
    }

    public void skipTurn()
    {
        hightlight.gameObject.SetActive(false);
        endUnitTurn();
        focusNextUnit();
    }

    public void buildFire()
    {
        selectedUnit.woodCount--;
        selectedUnit.tile.hasFire = true;
        endUnitTurn();
        focusNextUnit();

    }

    public void boilWater()
    {
        if (!Board.board.isAdjacentToWater())
            selectedUnit.dirtyWaterCount--;
        selectedUnit.cleanWaterCount++;
        
        MainUI.mainUI.updateUnitUI();
        StartCoroutine(waitForMove(0.5f));
    }

    public void takeWater()
    {
        selectedUnit.dirtyWaterCount++;
        endUnitTurn();
        MainUI.mainUI.updateUnitUI();
        endUnitTurn();
        StartCoroutine(waitForMove(0.5f));
    }

    void endUnitTurn()
    {
        //Debug.Log(turnOrderCount);
        selectedUnit.turnOrder = turnOrderCount;
        selectedUnit.turnOver = true;
        turnOrderCount++;
    }

    public void cookMeat()
    {
        selectedUnit.rawMeatCount--;
        selectedUnit.foodCount++;
        MainUI.mainUI.updateUnitUI();
        endUnitTurn();
        StartCoroutine(waitForMove(0.5f));
    }

}
