﻿using UnityEngine;
using System.Collections;

public enum materialEnum : int
{
    Black,
    White,
    Red,
    Green,
    Blue,
    Yellow,
    Orange,
    Teal
}


public class Helper : MonoBehaviour {

    public static Helper helper;
    Material[] materials;


    void Awake()
    {
        helper = this;
        loadMaterials();
    }

    public void loadMaterials()
    {
        materials = new Material[20];
        materials[(int)materialEnum.Black] = Resources.Load<Material>("Materials/Black");
        materials[(int)materialEnum.White] = Resources.Load<Material>("Materials/White");
        materials[(int)materialEnum.Red] = Resources.Load<Material>("Materials/Red");
        materials[(int)materialEnum.Green] = Resources.Load<Material>("Materials/Green");
        materials[(int)materialEnum.Blue] = Resources.Load<Material>("Materials/Blue");
        materials[(int)materialEnum.Yellow] = Resources.Load<Material>("Materials/Yellow");
        materials[(int)materialEnum.Orange] = Resources.Load<Material>("Materials/Orange");
        materials[(int)materialEnum.Teal] = Resources.Load<Material>("Materials/Teal");

    }

    public Material material(materialEnum material)
    {
        return materials[(int)material];
    }



}
