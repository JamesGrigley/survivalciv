﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public static Spawner spawner;

    Transform unitPrefab;
    Transform buildingPrefab;

    void Awake()
    {
        spawner = this;
        unitPrefab = Resources.Load<Transform>("Prefabs/BaseUnit");
        buildingPrefab = Resources.Load<Transform>("Prefabs/Building");
    }
    
    public BaseUnit spawnUnit(int ID, int playerID, int x, int y)
    {
        Transform unitTransform = (Transform)Instantiate(unitPrefab, Vector3.zero, Quaternion.identity);
        BaseUnit unit = unitTransform.GetComponent<BaseUnit>();
        unit.PlayerID = playerID;
        unit.ID = ID;
        Board.board.placeUnit(unit, x, y);
        return unit;
    }

    public BaseBuilding spawnBuilding(int x, int y)
    {
        Transform unitTransform = (Transform)Instantiate(buildingPrefab, Vector3.zero, Quaternion.identity);
        BaseBuilding building = unitTransform.GetComponent<BaseBuilding>();

        Board.board.placeBuilding(building, x, y);
        return building;
    }
}
