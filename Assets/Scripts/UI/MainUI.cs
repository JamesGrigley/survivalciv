﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {

    public static MainUI mainUI;

    Text turnText;
    Text unitNameText;
    Text woodCountText;
    Text cleanWaterText;
    Text dirtyWaterText;
    Text foodCountText;
    Text rawMeatCountText;

    Button scavengeButton;
    Button buildFireButton;
    Button boilWaterButton;
    Button takeWaterButton;
    Button cookMeatButton;

    void Awake()
    {
        mainUI = this;
        turnText = GameObject.Find("TurnText").GetComponent<Text>();
        unitNameText = GameObject.Find("UnitNameText").GetComponent<Text>();
        woodCountText = GameObject.Find("WoodCountText").GetComponent<Text>();
        cleanWaterText = GameObject.Find("CleanWaterText").GetComponent<Text>();
        dirtyWaterText = GameObject.Find("DirtyWaterText").GetComponent<Text>();
        foodCountText = GameObject.Find("FoodCountText").GetComponent<Text>();
        rawMeatCountText = GameObject.Find("RawMeatCountText").GetComponent<Text>();

        scavengeButton = GameObject.Find("ScavengeButton").GetComponent<Button>();
        buildFireButton = GameObject.Find("BuildFireButton").GetComponent<Button>();
        boilWaterButton = GameObject.Find("BoilWaterButton").GetComponent<Button>();
        takeWaterButton = GameObject.Find("TakeWaterButton").GetComponent<Button>();
        cookMeatButton = GameObject.Find("CookMeatButton").GetComponent<Button>();

        //toggleScavengeButton(false);
        //toggleScavengeButton(false);

    }

    void Start()
    {
        updateUnitUI();
    }

    public void updateTurnUI()
    {
        turnText.text = "Turn " + GameplayController.gameplayController.currentTurn.ToString();
    }

    public void updateUnitUI()
    {
        BaseUnit unit = UnitManager.unitManager.selectedUnit;
        if (unit != null)
        {
            unitNameText.text = unit.unitName;
            woodCountText.text = "Wood: " + unit.woodCount.ToString();
            dirtyWaterText.text = "Dirty Water: " + unit.dirtyWaterCount.ToString();
            cleanWaterText.text = "Clean Water: " + unit.cleanWaterCount.ToString();
            foodCountText.text = "Food: " + unit.foodCount.ToString();
            rawMeatCountText.text = "Raw Meat: " + unit.rawMeatCount.ToString();

            toggleScavengeButton((unit.tile.building));
            toggleBuildFireButton((unit.woodCount > 0));
            toggleBoilWaterButton(unit.tile.hasFire && (Board.board.isAdjacentToWater() || unit.dirtyWaterCount > 0));
            toggleTakeWaterButton(Board.board.isAdjacentToWater());
            toggleCookMeatButton(unit.tile.hasFire && unit.rawMeatCount > 0);
        }
        else
        {
            unitNameText.text = "";
            woodCountText.text = "";
        }
    }

    void toggleScavengeButton(bool toggle)
    {
        scavengeButton.gameObject.SetActive(toggle);
    }

    void toggleBuildFireButton(bool toggle)
    {
        buildFireButton.gameObject.SetActive(toggle);
    }

    void toggleBoilWaterButton(bool toggle)
    {
        boilWaterButton.gameObject.SetActive(toggle);
    }

    void toggleTakeWaterButton(bool toggle)
    {
        takeWaterButton.gameObject.SetActive(toggle);
    }

    void toggleCookMeatButton(bool toggle)
    {
        cookMeatButton.gameObject.SetActive(toggle);
    }

}
