﻿using UnityEngine;
using System.Collections;

public class GameplayController : MonoBehaviour {

    public static GameplayController gameplayController;
    [System.NonSerialized]
    public int currentTurn = 1;

	void Awake ()
    {
        gameplayController = this;
    }

    void Start()
    {
        Board.board.instantiateBoard();
        UnitManager.unitManager.loadStartUnits();
        MainUI.mainUI.updateTurnUI();
    }

    public void resetTurn()
    {
        currentTurn++;
        MainUI.mainUI.updateTurnUI();
        UnitManager.unitManager.newTurn();
        UnitManager.unitManager.focusNextUnit();
    }



}
